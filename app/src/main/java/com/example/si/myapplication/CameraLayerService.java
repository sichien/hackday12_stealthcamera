package com.example.si.myapplication;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.hardware.Camera;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import java.util.List;

public class CameraLayerService extends Service {
    public static Camera myCamera;
    private SurfaceHolder.Callback mSurfaceListener;
    View view;
    WindowManager wm;

    public CameraLayerService(){

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("sichien", "service→start：CameraLayerService");

        // Viewからインフレータを作成する
        LayoutInflater layoutInflater = LayoutInflater.from(this);

        // 重ね合わせするViewの設定を行う
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);

        // WindowManagerを取得する
        wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display disp = wm.getDefaultDisplay();
        Point point = new Point();
        disp.getSize(point);
        Log.d("sichien", String.valueOf(point.x));
        params.x = point.x/2;
        params.y = point.y/2;


        // レイアウトファイルから重ね合わせするViewを作成する
        view = layoutInflater.inflate(R.layout.overlay, null);

        SurfaceView mySurfaceView = (SurfaceView) view.findViewById(R.id.surface_view);
        SurfaceHolder holder = mySurfaceView.getHolder();
        holder.addCallback(mSurfaceListener);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        // Viewを画面上に重ね合わせする
        wm.addView(view, params);
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("sichien", "start: CameraLayerService");
        mSurfaceListener =
                new SurfaceHolder.Callback() {
                    public void surfaceCreated(SurfaceHolder holder) {
                        // TODO Auto-generated method stub
                        myCamera = Camera.open();
                        try {
                            myCamera.setPreviewDisplay(holder);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    public void surfaceDestroyed(SurfaceHolder holder) {
                        // TODO Auto-generated method stub
                        myCamera.release();
                        myCamera = null;
                    }

                    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                               int height) {
                        // TODO Auto-generated method stub
                        Camera.Parameters parameters = myCamera.getParameters();
                        List<Camera.Size> previewSizes = myCamera.getParameters().getSupportedPreviewSizes();
                        Camera.Size size = previewSizes.get(0);
                        parameters.setPreviewSize(size.width, size.height);
                        myCamera.setParameters(parameters);
                        myCamera.startPreview();
                    }
                };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // サービスが破棄されるときには重ね合わせしていたViewを削除する
        wm.removeView(view);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }
}
