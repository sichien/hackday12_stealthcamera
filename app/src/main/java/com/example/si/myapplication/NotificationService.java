package com.example.si.myapplication;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.text.GetChars;
import android.util.Log;

import java.io.FileOutputStream;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class NotificationService extends NotificationListenerService {
    private String TAG = this.getClass().getSimpleName();
    private int count = 0;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        Log.d("sichien", sbn.getPackageName());
        if (sbn.getPackageName().contains("ifttt")) {
            Log.d("sichien", "通知検知!!!" + "： from ifttt");

            Camera myCamera = CameraLayerService.myCamera;

            if (myCamera != null) {
                myCamera.takePicture(mShutterListener, null, mPictureListener);
            }
        }
    }

    // シャッターが押されたときに呼ばれるコールバック
    private Camera.ShutterCallback mShutterListener =
            new Camera.ShutterCallback() {
                public void onShutter() {
                    // TODO Auto-generated method stub
                }
            };

    // JPEGイメージ生成後に呼ばれるコールバック
    private Camera.PictureCallback mPictureListener =
            new Camera.PictureCallback() {
                public void onPictureTaken(byte[] data, Camera camera) {
                    // SDカードにJPEGデータを保存する
                    if (data != null) {
                        Log.d("sichien", data.toString());
                        FileOutputStream myFOS = null;
                        try {
                            String path = Environment.getExternalStorageDirectory().getPath();
                            Log.d("sichien", path);

                            myFOS = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/tmp/" + "camera_test" + count++ + ".jpg");
                            myFOS.write(data);
                            myFOS.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        camera.startPreview();
                    }
                }
            };
}
