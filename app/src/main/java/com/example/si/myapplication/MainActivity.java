package com.example.si.myapplication;

import android.content.ComponentName;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

import java.io.FileOutputStream;
import java.util.List;


public class MainActivity extends ActionBarActivity {
    public static Camera myCamera;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // notification serviceの起動
        startService(new Intent(this, NotificationService.class));


        Button startButton = (Button) findViewById(R.id.camera_start);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // cameraLayer serviceの起動
                Log.d("sichien", "start");
                ComponentName comName = startService(new Intent(MainActivity.this, CameraLayerService.class));
                Log.d("sichien", comName.toShortString());
            }
        });
        Button stopButton = (Button) findViewById(R.id.camera_stop);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("sichien", "stop");
                // cameraLayer serviceの起動
                stopService(new Intent(MainActivity.this, CameraLayerService.class));
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
